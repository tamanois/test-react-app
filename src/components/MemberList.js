import React from 'react';
import Member from './Member';

import MemberObject from '../objects/Member';
import MemberModal from './modals/MemberModal';
import {on, off, EventList, trigger} from '../objects/Events';
import MyFirebase from './firebase/firebase';
import M from 'materialize-css';

class MemberList extends React.Component {

   
    members = [];
    memberModalId = 'create-member-modal';
    constructor(props) {
        super(props);
       
        this.state = {
            members : this.members
        }
        this.memberAdded = this.memberAdded.bind(this);
        this.getAllMembers = this.getAllMembers.bind(this);
        this.refreshMemberList = this.refreshMemberList.bind(this);

        this.db = MyFirebase.getDB();
       
    }

    
    /**
     *  Get all the members on firebase
     */
    getAllMembers() {
        try{ 
            trigger(EventList.SHOW_LOADING_SCREEN);
            this.members = [];
            var m;

            this.db.collection('members').get().then((documents) => {

                documents.forEach((doc) => {
                    m = new MemberObject(doc.id, doc.data().firstname, doc.data().lastname, doc.data().age, doc.data().gender, doc.data().presentation);
                    this.members.push(m);
                    this.setState({members : [m]})

                });

                //console.log('MEMBERS', this.members);
                this.setState({members : this.members})
                trigger(EventList.HIDE_LOADING_SCREEN);
            })
        }
        catch (error) {
            this.errorHandler(error);
        }
        
       
    }

    /**
     * Refresh the members list after some update occurs
     */
    refreshMemberList() {
        this.getAllMembers()
    };

    errorHandler(error) {
        M.toast({html: 'Une erreur est survenue', classes : 'red lighten-1'});
        console.error("error" , error);
  
      }

  
    componentDidMount() {
        on(EventList.MEMBER_CREATED, this.memberAdded);
        on(EventList.REFRESH_MEMBER_LIST, this.refreshMemberList);

        this.getAllMembers();
    }
    componentWillUnmount () {
        off(EventList.MEMBER_CREATED, this.memberAdded);
        off(EventList.REFRESH_MEMBER_LIST, this.refreshMemberList);

    }

    /**
     * Triggered when a member is added
     */
    memberAdded(member) {
        this.members.push(member.detail);
        this.setState({members : this.members})
    }

    render() {
        return (
            <div>
                <div class="row left-align">
                   <MemberModal titleName={"Nouveau membre"} modalId={this.memberModalId}>
                        <button data-target={this.memberModalId}   class="waves-effect waves-light btn purple darken-3 modal-trigger"><i class="material-icons left">add</i>Ajouter un membre</button>
                    </MemberModal> 

                </div>
                <div class="row">
                {this.state.members.map(
                    (member, i) => (
                        <Member key={member.id} member={member}/> 
                    )
                )}
                </div>

           
            </div>
            
        );
    }
}

export default MemberList;
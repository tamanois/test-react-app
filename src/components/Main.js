import React from 'react';
import MemberList from './MemberList';

class Main extends React.Component {


    render() {
        return (
            <div>
                <div className="container" style={{paddingTop : '15px'}}>
                    <MemberList/>
                </div>
            </div>
           
        );
    }
}

export default Main;
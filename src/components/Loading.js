import React from 'react';
import loadingIcon from '../assets/images/loading.gif';
import { EventList, off, on } from '../objects/Events';


export default class Loading extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading : false
        }
        this.startLoading = this.startLoading.bind(this);
        this.stopLoading = this.stopLoading.bind(this);

    }

    componentDidMount() {
        on(EventList.SHOW_LOADING_SCREEN, this.startLoading);
        on(EventList.HIDE_LOADING_SCREEN, this.stopLoading);

    }

    componentWillUnmount() {
        off(EventList.SHOW_LOADING_SCREEN, this.startLoading);
        off(EventList.HIDE_LOADING_SCREEN, this.stopLoading);
     }    

    startLoading() {
        this.setState({isLoading : true});
    }

    stopLoading() {
        this.setState({isLoading : false});
    }
    render() {
        if(this.state.isLoading) {
            return (<div class="loading-back">
                        <img class="loading-icon"  src={loadingIcon} alt="loading icon"></img>
                    </div>)
        } else {
            return (<div></div>)
        }
       
    }
}
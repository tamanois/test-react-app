import app from 'firebase/app';
import firestore from 'firebase/firestore';
const config = {
    apiKey: "AIzaSyB3XOSr03JpYWjVJDlCWVnKPlBhyzvAY2g",
    authDomain: "test-react-app-72567.firebaseapp.com",
    projectId: "test-react-app-72567",
    storageBucket: "test-react-app-72567.appspot.com",
    messagingSenderId: "365537078524",
    appId: "1:365537078524:web:4b47211c583a634f277b79",
    measurementId: "G-W5NT0ECRZ9"
  };


export default class MyFirebase  {

    /**
     * Get the firestore db instance
     */
    static getDB() {
        if (!app.apps.length) {
            app.initializeApp(config);
         }else {
            app.app(); // if already initialized, use that one
         }

        return app.firestore();
    }
    
}
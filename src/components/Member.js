import React from 'react';
import img1 from '../assets/images/user-flat.jpg';
import MemberModal from './modals/MemberModal';
import M from 'materialize-css';
import MyFirebase from './firebase/firebase';
import { EventList, trigger } from '../objects/Events';


export default class Member extends React.Component {

    member = this.props.member;

    memberModalId = 'update-member-modal';

    constructor(props) {
        super(props);

        this.deleteMember = this.deleteMember.bind(this);
    }

    componentDidMount() {
        //get DB instance
        this.db = MyFirebase.getDB();
    }

    /**
     * 
     * Delete member in firebase via his ID
     */
    deleteMember(memberId) {
        trigger(EventList.SHOW_LOADING_SCREEN);        
        try { 
          this.db.collection('members').doc(memberId).delete().then(() => {
            console.log("Document successfully deleted!");
            this.operationSuccessNotification();
            // Trigger an event to refresh member list
            trigger(EventList.REFRESH_MEMBER_LIST);
            
            //trigger(EventList.HIDE_LOADING_SCREEN);

    
        }).catch((error) => {
            console.error("Error removing document: ", error);
            this.errorHandler(error);
            trigger(EventList.HIDE_LOADING_SCREEN);

  
        });
        }
         catch(error) {
        trigger(EventList.HIDE_LOADING_SCREEN);
        this.errorHandler(error);
      }
        
      }

      

    operationSuccessNotification() {
        M.toast({html: 'Opération effectuée', classes : 'green lighten-1'});
  
      }
  
      componentDidCatch(error, info) {
       this.errorHandler(error);
      }
  
      errorHandler(error) {
        M.toast({html: 'Une erreur est survenue', classes : 'red lighten-1'});
        console.error("error" , error);
  
      }

    render() {
        return (
            <div className="col s6 m4 " style={{paddingLeft : '10px', paddingRight : '10px'}}>
                <div class="card hoverable left-align">
                    <div class="card-image">
                        <img src={img1} alt="Membre"/>
                        <span class="card-title">{this.member.firstname + ' ' + this.member.lastname}</span>
                    </div>
                    <div class="card-content">
                        <p className="indigo-text darken-3"> <i class="material-icons">date_range</i> {this.member.age} ans</p>
                        <p className="indigo-text darken-3"> <i class="material-icons">person</i>  {this.member.gender === 'H'? 'Homme' : 'Femme' }</p>

                        <p className="grey-text">{this.member.presentation}</p>
                    </div>
                    <div class="card-action">
                        
                    <MemberModal titleName={"Modifier un membre"} isUpdate={true} modalId={this.memberModalId + this.member.id} member={this.member}>
                        <button class="btn-floating btn waves-effect waves-light blue darken-1 modal-trigger"  data-target={this.memberModalId + this.member.id}><i class="material-icons">edit</i></button>
                    </MemberModal> 
                        <button class="btn-floating btn waves-effect waves-light red"  onClick={() => this.deleteMember(this.member.id)}><i class="material-icons">delete</i></button>

                    </div>
                </div>
            </div>
        );
    }
}


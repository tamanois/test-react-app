import './App.css';
import Header from './components/Header';
import Main from './components/Main';
import Loading from './components/Loading';

function App() {
  return (
    <div className="App">
      <Loading />
      <Header />
      <Main />
    </div>
  );
}

export default App;

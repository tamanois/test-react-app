import React from 'react';
import M from 'materialize-css';
import MemberObject from '../../objects/Member';
import { EventList, trigger } from '../../objects/Events';
import MyFirebase from '../firebase/firebase';

export default class MemberForm extends React.Component {

    isUpdate =  false;

    constructor(props) {
        super(props);
        this.old_member = this.props.member;
        this.db = MyFirebase.getDB();

        if(props.isUpdate && this.old_member) {
            console.log('isUpdate');
            this.state = {
                firstname : this.old_member.firstname,
                lastname : this.old_member.lastname,
                age : this.old_member.age,
                gender : this.old_member.gender,
                presentation : this.old_member.presentation,

            };
        } else {
            this.state = {
              gender : 'H',
            };
        }
        
    
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateMember = this.updateMember.bind(this);

        this.isUpdate = props.isUpdate;

      }


    
      handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
        
      }

      handleSubmit(event) {
        event.preventDefault();
        var m = new MemberObject(null, this.state.firstname, this.state.lastname, this.state.age, this.state.gender, this.state.presentation)
        if(this.isUpdate && this.old_member) {
            m.id = this.old_member.id;
        }
        //console.log('member', m);
        
        if(this.isUpdate) {
          this.updateMember(m)
        } else {
          this.createMember(m);
        }

    }

    /**
     * Create member in firebase db
     */
    createMember(member) {
      try{
            trigger(EventList.SHOW_LOADING_SCREEN);
            this.db.collection("members").add({
                  firstname : member.firstname,
                  lastname : member.lastname,
                  age : member.age,
                  gender : member.gender,
                  presentation : member.presentation,
          
                })
            .then((docRef) => {
            //console.log("Document written with ID: ", docRef.id);
            member.id = docRef.id;
            this.props.operationTerminatedHandler();
            this.operationSuccessNotification();
            trigger(EventList.MEMBER_CREATED, member);
            trigger(EventList.HIDE_LOADING_SCREEN);
                })
            .catch((error) => {
              this.errorHandler();
              trigger(EventList.HIDE_LOADING_SCREEN);
                console.error("Error adding document: ", error);
            });

  } catch(error) {
    trigger(EventList.HIDE_LOADING_SCREEN);
    this.errorHandler(error);
  }
  
    }

    /**
     * Update member in firebase db
     */
    updateMember(member) {
      trigger(EventList.SHOW_LOADING_SCREEN);        
      try { 
        this.db.collection('members').doc(member.id).update(
          {
            firstname : member.firstname,
            lastname : member.lastname,
            age : member.age,
            gender : member.gender,
            presentation : member.presentation
          }
        )
        .then(() => {
          console.log("Document successfully updated!");
          this.operationSuccessNotification();
          this.props.operationTerminatedHandler();
          trigger(EventList.REFRESH_MEMBER_LIST);
          //trigger(EventList.HIDE_LOADING_SCREEN);

  
      }).catch((error) => {
          console.error("Error updating document: ", error);
          this.errorHandler(error);
          trigger(EventList.HIDE_LOADING_SCREEN);


      });
      }
       catch(error) {
      trigger(EventList.HIDE_LOADING_SCREEN);
      this.errorHandler(error);
    }
      
    }


   
    

    operationSuccessNotification() {
      M.toast({html: 'Opération effectuée', classes : 'green lighten-1'});

    }

    componentDidCatch(error, info) {
     this.errorHandler(error);
    }

    errorHandler(error) {
      M.toast({html: 'Une erreur est survenue', classes : 'red lighten-1'});
      console.error("error" , error);

    }

    componentDidMount() {
        M.AutoInit();
    }




    render () {
        return  (
            <div class="row">
            <form class="col s12" onSubmit={this.handleSubmit} id="member-form">
              <div class="row">
                <div class="input-field col s6">
                  <input id="firstname" name="firstname" type="text" class="validate" required onChange={this.handleInputChange} defaultValue={this.props.member ? this.props.member.firstname : ''}/>
                  <label for="firstname" className={this.props.member ? 'active' : ''}>Prénom</label>
                </div>
                <div class="input-field col s6">
                  <input id="lastname" name="lastname" type="text" class="validate" required onChange={this.handleInputChange} defaultValue={this.props.member ? this.props.member.lastname : ''}/>
                  <label for="lastname" className={this.props.member ? 'active' : ''}>Nom</label>
                </div>
                <div class="input-field col s6">
                  <input id="age" name="age" type="number" min="0" step="1" class="validate" required onChange={this.handleInputChange} defaultValue={this.props.member ? this.props.member.age : ''}/>
                  <label for="age" className={this.props.member ? 'active' : ''}>Âge</label>
                </div>
                <div class="input-field col s12">
                  <select form="member-form" name="gender" required onChange={this.handleInputChange} defaultValue={this.props.member ? this.props.member.gender : 'H'}>
                    <option value="" disabled>Choisissez une option</option>
                    <option value="H">Homme</option>
                    <option value="F">Femme</option>
                  </select>
                  <label>Genre</label>
                </div>

                <div class="input-field col s12">
                  <textarea required name="presentation" class="materialize-textarea" onChange={this.handleInputChange} defaultValue={this.props.member ? this.props.member.presentation : ''}></textarea>
                  <label className={this.props.member ? 'active' : ''}> Texte de présentation</label>
                </div>
              </div>

            <div className="row">
            <button class="btn waves-effect green darken-1 right"><i class="material-icons">save</i> Enregistrer</button>

            </div>
            </form>
          </div>
        )
        
     }
}
import React from 'react';
import M from "materialize-css";
import MemberForm from '../forms/MemberForm';

export default class MemberModal extends React.Component {

    constructor(props) {
        super(props);
        this.closeModal = this.closeModal.bind(this);
    }
    isUpdate = false;
    componentDidMount() {
        
        M.Modal.init(this.Modal);
      }



      closeModal() {
          var instance = M.Modal.getInstance(this.Modal);
          instance.close();
      }
   
     
    render() {

       
        return (
            <div style={{display : 'inline'}}>
                {this.props.children}

            <div  ref={Modal => { this.Modal = Modal; }} id={this.props.modalId} class="modal modal-fixed-footer">
                <div class="modal-content">
                <h4>{this.props.titleName}</h4>
                <MemberForm operationTerminatedHandler={this.closeModal} isUpdate={this.props.isUpdate ? true : undefined } member={this.props.member ? this.props.member : undefined } />
                </div>
                <div class="modal-footer">
                <button class="modal-close waves-effect btn red lighten-1">Fermer</button>
                </div>
            </div>
            </div>
            
        );
    }
}


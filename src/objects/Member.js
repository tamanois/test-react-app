export default class MemberObject {
     id;
     firstname;
     lastname;
     age;
     gender;
     presentation;

     constructor(id, firstname, lastname, age, gender, presentation) {
          this.id = id;
          this.firstname = firstname;
          this.lastname = lastname;
          this.age = age;
          this.gender = gender;
          this.presentation = presentation;
     }

     
}
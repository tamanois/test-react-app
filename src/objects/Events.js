/**
 * 
 * Register a callback to an event
 */
function on(eventType, listener) {
    document.addEventListener(eventType, listener);
  }

  /**
   * Unregister a callback to an event
   */
  function off(eventType, listener) {
    document.removeEventListener(eventType, listener);
  }

  /**
   * Trigger an event with data
   */
  function trigger(eventType, data) {
    const event = new CustomEvent(eventType, { detail: data });
    document.dispatchEvent(event);
  }

  export { on, off, trigger };

  export const  EventList = {
    SHOW_LOADING_SCREEN : 'start_loading_action',
    HIDE_LOADING_SCREEN : 'stop_loading_action',
    MEMBER_CREATED : 'memberform:member_created',
    REFRESH_MEMBER_LIST : 'refresh_member_list',

  }